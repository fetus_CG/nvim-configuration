local opts = {noremap=true,silent=true}

local term_opts = {silent = true}

local _keymap = vim.api.nvim_set_keymap

-- 设置空格为主键
_keymap("", "<Space>", "<Nop>", opts)
vim.g.mapleader=" "
vim.g.maplocalleader= " "

-- vim modes
-- 一般模式：n
-- 插入模式：i
-- 视图模式：v
-- 视图块模式：x
-- term模式：t
-- 命令模式：c

-- 一般模式
-- 当前聚焦窗口键位绑定替换
_keymap("n", "<C-h>","<C-w>h",opts)
_keymap("n", "<C-j>","<C-w>j",opts)
_keymap("n", "<C-k>","<C-w>k",opts)
_keymap("n", "<C-l>","<C-w>l",opts)

_keymap("n","<leader>e", ":Lex 20<CR>",opts)

-- 通过箭头按键resize
_keymap("n","<C-Up>",":resize +2<CR>",opts)
_keymap("n","<C-Down>",":resize -2<CR>",opts)
_keymap("n","<C-Left>",":vertical resize -2<CR>",opts)
_keymap("n","<C-Right>",":vertical resize +2<CR>",opts)

-- Navigate Buffer
_keymap("n", "<S-l>", ":bnext<CR>",opts)
_keymap("n", "<S-h>", ":bprevious<CR>",opts)

-- 插入模式
-- 改为jk键退出提高输入速度
-- _keymap("i", "<C-C>", "<ESC>",opts)

-- 视图模式
-- 停留在缩进模式
_keymap("v", "<","<gv",opts)
_keymap("v",">",">gv",opts)

-- 移动当前行
_keymap("v", "p","'_dP",opts)
_keymap("v","<A-j>",":m '>+1<CR>gv=gv",opts) 
_keymap("v", "<A-k>", ":m '<-2<CR>gv=gv", opts) 

-- 视图块模式
-- 当前行上下移动
_keymap("x","J",":move '>+1<CR>gv-gv",opts)
_keymap("x","K",":move '<-2<CR>gv-gv",opts)
_keymap("x","<A-j>",":move '>+1<CR>gv-gv",opts)
_keymap("x","<A-k>",":move '<-2<CR>gv-gv",opts)

-- 终端模式
-- 更好的终端引导
_keymap("t", "<C-h>", "<C-\\><C-N><C-w>h", term_opts)
_keymap("t", "<C-j>", "<C-\\><C-N><C-w>j", term_opts)
_keymap("t", "<C-k>", "<C-\\><C-N><C-w>k", term_opts)
_keymap("t", "<C-l>", "<C-\\><C-N><C-w>l", term_opts)

-- Telescope
_keymap("n", "<leader>f", "<cmd>lua require'telescope.builtin'.find_files(require('telescope.themes').get_dropdown({ previewer = false }))<cr>", opts)
_keymap("n", "<leader>h", "<cmd>Telescope live_grep<cr>", opts)
