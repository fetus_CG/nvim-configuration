-- 插件集合
local plugins = {
  -- git
  {
    "tpope/vim-fugitive"
  },
  {
    "lewis6991/gitsigns.nvim"
  },
  -- 注释
  {
    'JoosepAlviste/nvim-ts-context-commentstring'
  },
  {
    "numToStr/Comment.nvim"
  },
  -- 括号补全
  {
    "windwp/nvim-autopairs"
  },
  -- 高亮
  {
    "nvim-treesitter/nvim-treesitter",
    build = ":TSUpdate",
  },
  -- 搜索
  {
    "nvim-telescope/telescope.nvim"
  },
  -- LSP
  {
    "neovim/nvim-lspconfig"
  },
  {
    "williamboman/mason.nvim"
  },
  {
    "williamboman/mason-lspconfig.nvim"
  },
  {
    "jose-elias-alvarez/null-ls.nvim"
  },
  -- 自动补全
  {
    "hrsh7th/nvim-cmp"
  },
  {
    "hrsh7th/cmp-buffer" -- buffer补全
  },
  {
    "hrsh7th/cmp-path" -- 路径补全
  },
  {
    "hrsh7th/cmp-cmdline" -- cmd补全
  },
  {
    "saadparwaiz1/cmp_luasnip" -- lua snippet补全
  },
  {
    "L3MON4D3/LuaSnip"
  },
  {
    "rafamadriz/friendly-snippets"
  },
  -- 扩展colorscheme
  {
    "lunarvim/colorschemes"
  },
  {
    "folke/tokyonight.nvim"
  },
  {
    "sainnhe/sonokai"
  },
  {
    "morhetz/gruvbox"
  },
  {
    "Mofiqul/vscode.nvim"
  },
  {
    "tanvirtin/monokai.nvim"
  },
  {
    "rebelot/kanagawa.nvim"
  },
  {
    "catppuccin/nvim", name = "catppuccin", priority = 1000
  },
  -- 通用lua功能插件，一些主流的插件会用到该依赖
  {
    "nvim-lua/plenary.nvim"
  },
  -- 弹出窗口插件，一些主流的插件会用到该依赖
  {
    "nvim-lua/popup.nvim",
  },
}


-- lazy bootstrap
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable", -- latest stable release
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)

require("lazy").setup(plugins)
