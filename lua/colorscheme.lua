
local default_color = "elflord"

-- colorscheme列表
local random_color = {
  "catppuccin",
  "catppuccin-frappe",
  "catppuccin-latte",
  "catppuccin-macchiato",
  "catppuccin-mocha",
  "codemonkey",
  "elfloard",
  "gruvbox",
  "kanagawa",
  "monokai",
  "tokyonight",
  "vscode",
}

-- 每次加载配置从表中随机获取一款皮肤
math.randomseed(os.time())
local color = random_color[math.random(12)]

local ok, _ = pcall(vim.cmd, "colorscheme " .. color)
if not ok then
  vim.notify("colorscheme " .. color .. " not found!")
  return
end
