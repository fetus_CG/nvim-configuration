local lsp_servers = {
  "lua_ls",
  "jsonls",
}

local settings = {
  ui = {
    border = "none",
    icons = {
      package_installed = "已安装",
      package_pending = "安装中",
      package_uninstalled = "未安装"
    },
  },
  log_level = vim.log.levels.INFO,
  max_concurrent_installers = 4
}

require("mason").setup(settings)
require("mason-lspconfig").setup({
  ensure_installed = lsp_servers,
  automatic_installation = true,
})

local lspconfig_ok, lspconfig = pcall(require, "lspconfig")
if not lspconfig_ok then
  return
end

local opts = {}

for _, server in pairs(lsp_servers) do
  opts = {
    on_attach = require("lsp.handlers").on_attach,
    capabilities = require("lsp.handlers").capabilities
  }

  server = vim.split(server, "@")[1]
  local require_ok, conf_opts = pcall(require, "lsp.settings." .. server)
  if require_ok then
    opts = vim.tbl_deep_extend("force", conf_opts, opts)
  end

  lspconfig[server].setup(opts)
end
