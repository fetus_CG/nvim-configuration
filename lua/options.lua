local _opt = vim.opt
-- :help options
_opt.backup = false -- 创建备份文件
_opt.clipboard = "unnamedplus" -- 允许nvim访问系统剪贴板
_opt.cmdheight = 2 -- nvim cmd模式下展示更多的信息
_opt.completeopt = { "menuone" , "noselect" } -- 适配cmp
_opt.conceallevel = 0 -- ··在markdown中可见
_opt.fileencoding = "utf-8" -- 文件编码格式
_opt.hlsearch = true -- 高亮搜索内容
_opt.ignorecase = true -- 搜索时忽略大小写
_opt.mouse = "a" -- 允许在nvim中使用鼠标
_opt.pumheight = 10 -- 弹出菜单的高度
_opt.showmode = true -- 是否显示诸如 -- VISUAL -- 这种模式提示信息
_opt.showtabline=2 -- 显示tabs
_opt.smartcase=true -- smart case
_opt.smartindent=true -- smart indent
_opt.splitbelow=true -- 水平分割窗口时新窗口在下方生成
_opt.splitright=true -- 垂直分割窗口时新窗口在右侧生成
_opt.swapfile=false -- 创建swapfile
_opt.termguicolors=true -- 允许gui颜色主题（可不用配置，大部分终端都支持）
_opt.timeoutlen=1000 -- 映射结果完成的最大等待时间
_opt.undofile=true -- 启用撤回
_opt.updatetime=300 -- 更快的completion，默认4000
_opt.writebackup=false -- 如果一个文件被另一个程序编辑了，这边更新
_opt.expandtab=true -- tabs变为空格
_opt.shiftwidth=2 -- 缩进空格数
_opt.tabstop =2 -- 按一次tab两个空格
_opt.cursorline=true -- 高亮当前行
_opt.number = true -- 显示行号
_opt.relativenumber=false -- 显示相对行号
_opt.numberwidth=4 -- 行号列宽度
_opt.signcolumn="yes" -- 永远显示sign column，否则每次移动文本
_opt.wrap=false -- 一行文本不换行显示
_opt.scrolloff=8 -- 横向滚动长度
_opt.sidescrolloff=8 -- 纵向滚动长度
_opt.guifont="monospace:h17" -- nvim字体

_opt.shortmess:append "c"

