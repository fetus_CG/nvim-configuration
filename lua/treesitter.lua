local ok, configs = pcall(require, "nvim-treesitter.configs")
if not ok then
  return
end

configs.setup({
  ensure_installed = {
    "lua",
    "vim"
  },
  sync_install = false,
  ignore_install = { "" },
  highlight = {
    enable = true,
    disable = { "" },
    addtional_vim_regex_highlighting = true,
  },
  indent = {
    enable = true,
    disable = {
      "yaml"
    }
  },
})
